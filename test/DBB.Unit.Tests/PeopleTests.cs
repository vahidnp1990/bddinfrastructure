﻿using BDD.Core.Domain;
using BDD.Core.DomainSarvices.People;
using BDD.Core.DomainSarvices.People.Contracts;
using BDD.Core.DomainSarvices.People.Contracts.Dtos;
using BDD.Core.DomainSarvices.People.Exceptions;
using BDD.Infrastructure.Persistence;
using DBB.Unit.Tests.Infrastructure;
using FluentAssertions;
using Xunit;

namespace DBB.Unit.Tests
{
    public class PeopleTests : UnitTestPersistence<EFDataContext>
    {
        private readonly IPeopleService _sut;

        public PeopleTests()
        {
            var repository = new PeopleRepository(Context);
            _sut = new PeopleService(repository);
        }

        [Fact]
        private async Task Add_added_person()
        {
            var dto = new AddPersonDto
            {
                Name = "dummy_name"
            };

            await _sut.Add(dto);

            var person = ReadDataContext.People.SingleOrDefault();

            person.Should().NotBeNull();
            person!.Name.Should().Be(dto.Name);
        }

        [Fact]
        private async Task Update_changed_person()
        {
            var person = new Person
            {
                Name = "dummy_name",
                Description = "dummy_description"
            };
            Save(person);

            var dto = new UpdatePersonDto
            {
                Description = "update_dummy_Description",
                Name = "update_dummy_name"
            };

            await _sut.Update(person.Id, dto);

            var personExpected = ReadDataContext.People.SingleOrDefault();
            personExpected.Should().NotBeNull();
            personExpected!.Name.Should().Be(dto.Name);
            personExpected.Description.Should().Be(dto.Description);
        }

        [Fact]
        private async Task Update_throw_exception_when_person_not_found()
        {
            var fackPersonId = -1;
            var dto = new UpdatePersonDto
            {
                Description = "update_dummy_Description",
                Name = "update_dummy_name"
            };

            var expected = async () => await _sut.Update(fackPersonId, dto);

            await expected.Should().ThrowAsync<PersonNotFoundException>();
        }

        [Fact]
        private async Task ChangeName_changed_person_name()
        {
            var person = new Person
            {
                Name = "dummy_name",
                Description = "dummy_description"
            };
            Save(person);

            var dto = new UpdatePersonNameDto
            {
                Name = "update_dummy_name"
            };

            await _sut.ChangeName(person.Id, dto);

            var personExpected = ReadDataContext.People.SingleOrDefault();
            personExpected.Should().NotBeNull();
            personExpected!.Name.Should().Be(dto.Name);
        }

        [Fact]
        private async Task ChangeName_throw_exception_when_person_not_found()
        {
            var fackPersonId = -1;
            var dto = new UpdatePersonNameDto
            {
                Name = "update_dummy_name"
            };

            var expected = async () => await _sut.ChangeName(fackPersonId, dto);

            await expected.Should().ThrowAsync<PersonNotFoundException>();
        }
        protected void Save<T>(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Context.Manipulate(_ => _.Add(entity));
        }
    }



}
