﻿using Xunit;

namespace BDD.Spec.Tests.Infrastructure
{
    [CollectionDefinition(nameof(ConfigurationFixture<Program>), DisableParallelization = true)]
    public class ConfigurationCollectionFixture : ICollectionFixture<ConfigurationFixture<Program>>
    {
    }

}
