﻿using BDD.Infrastructure.Persistence;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;

namespace BDD.Spec.Tests.Infrastructure
{

    public class EFDataContextDatabaseFixture<TContext> where TContext : DbContext
    {
        public readonly ConfigurationFixture<Program> _configuration;
        public TContext Context = default!;
        public TContext ReadContext = default!;
        public EFDataContextDatabaseFixture(ConfigurationFixture<Program> configuration)
        {
            _configuration = configuration;
            Context = CreateDataContext();
            ReadContext = CreateDataContext();
        }

        protected TContext CreateDataContext()
        {
            var option = new DbContextOptionsBuilder<EFDataContext>()
          .UseSqlServer(_configuration.Value.ConnectionString).Options;
            var context = Activator.CreateInstance(typeof(TContext), new object[] {
                option }) as TContext;
            if (context == null)
            {
                throw new NullReferenceException();
            }
            return context;
        }
        public void Save<T>(T entity) where T : class
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            Context.Add(entity);
            Context.SaveChanges();
        }

    }

    public class RestFixture
    {
        protected string JwtToken = default!;
        protected readonly HttpClient _httpClient;
        public RestFixture(ConfigurationFixture<Program> configuration) {
            JwtToken = GenerateJwt(configuration.Value);
            _httpClient = configuration.CreateClient();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JwtToken);
        }

        private string GenerateJwt(PersistenceConfig config)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.JwtKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //If you've had the login module, you can also use the real user information here
            var claims = new[] {
              new Claim(JwtRegisteredClaimNames.Sub, "user_name"),
              new Claim(JwtRegisteredClaimNames.Email, "user_email"),
              new Claim("DateOfJoing", "2022-09-12"),
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
             };

            var token = new JwtSecurityToken(config.JwtIssuer,
                config.JwtIssuer,
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        protected async Task<TResult?> Get<TResult>(string url)
        {
            var respon = await _httpClient.GetAsync(url);
            CheckResponseSuccess(respon);
            return await DeserializeObject<TResult>(respon);
        }

        protected async Task<TResult?> Post<TResult>(string url, object dto)
        {
            var respon = await PostOjbect(url, dto);

            return await DeserializeObject<TResult>(respon);
        }

        protected async Task Post(string url, object dto)
        {
            await PostOjbect(url, dto);
        }

        protected async Task<TResult?> Put<TResult>(string url, object dto)
        {
            var respon = await PutObejct(url, dto);

            return await DeserializeObject<TResult>(respon);
        }
        protected async Task Put(string url, object dto)
        {
            await PutObejct(url, dto);
        }

        protected async Task<TResult?> Patch<TResult>(string url, object dto)
        {
            var respon = await PatchObject(url, dto);

            return await DeserializeObject<TResult>(respon);
        }

        protected async Task Patch(string url, object dto)
        {
            await PatchObject(url, dto);
        }

        protected async Task Delete(string url)
        {
            var respon = await _httpClient.DeleteAsync(url);
            CheckResponseSuccess(respon);
        }

        private async Task<HttpResponseMessage> PutObejct(string url, object dto)
        {
            var stringContent = SerializeObject(dto);
            var respon = await _httpClient.PutAsync(url, stringContent);
            CheckResponseSuccess(respon);
            return respon;
        }

        private async Task<HttpResponseMessage> PatchObject(string url, object dto)
        {
            var stringContent = SerializeObject(dto);
            var respon = await _httpClient.PatchAsync(url, stringContent);
            CheckResponseSuccess(respon);
            return respon;
        }

        private async Task<HttpResponseMessage> PostOjbect(string url, object dto)
        {
            var stringContent = SerializeObject(dto);
            var respon = await _httpClient.PostAsync(url, stringContent);
            CheckResponseSuccess(respon);
            return respon;
        }

        private static async Task<TResult?> DeserializeObject<TResult>(HttpResponseMessage respon)
        {
            var values = await respon.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TResult>(values);
        }

        private static StringContent SerializeObject(object dto)
        {
            var bodyJson = JsonConvert.SerializeObject(dto);
            var stringContent = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            return stringContent;
        }

        private static void CheckResponseSuccess(HttpResponseMessage respon)
        {
            respon.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
        }
    }
}
