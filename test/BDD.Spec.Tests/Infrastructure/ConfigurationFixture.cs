﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;

namespace BDD.Spec.Tests.Infrastructure
{
    public class ConfigurationFixture<TProgram>
     : WebApplicationFactory<TProgram> where TProgram : class
    {
        private const string PersistenceConfigKey = "PersistenceConfig";
        private const string AppSettingPath = "appsettings.json";
        private IConfiguration _configuration = default!;
        public PersistenceConfig Value { get; private set; }

        public ConfigurationFixture()
        {
            Value = GetSettings();

        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
            });

            builder.UseEnvironment("Development");
            builder.UseConfiguration(_configuration);
        }
        private PersistenceConfig GetSettings()
        {
            _configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile(AppSettingPath, optional: true, reloadOnChange: false)
               .AddEnvironmentVariables()
               .AddCommandLine(Environment.GetCommandLineArgs())
               .Build();
            var settings = new PersistenceConfig();
            _configuration.Bind(PersistenceConfigKey, settings);
            return settings;
        }
    }

}
