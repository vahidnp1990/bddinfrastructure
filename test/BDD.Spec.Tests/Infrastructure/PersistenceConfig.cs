﻿namespace BDD.Spec.Tests.Infrastructure
{
    public class PersistenceConfig
    {
        public string ConnectionString { get; set; } = default!;
        public string JwtKey { get; set; } = default!;
        public string JwtIssuer { get; set; } = default!;
    }

}
