﻿using Microsoft.EntityFrameworkCore;
using Respawn;
using Xunit;

namespace BDD.Spec.Tests.Infrastructure
{
    [Collection(nameof(ConfigurationFixture<Program>))]
    public class SpecPersistence : RestFixture, IAsyncLifetime
    {
        private readonly ConfigurationFixture<Program> _configuration;
        protected Respawner _respawner = default!;
        public SpecPersistence(ConfigurationFixture<Program> configuration) : base(configuration)
        {
            _configuration = configuration;
        }

        public EFDataContextDatabaseFixture<T> CreateDataBase<T>() where T : DbContext
        {
            return new EFDataContextDatabaseFixture<T>(_configuration);
        }
        public async Task DisposeAsync()
        {
            if (IsTheConnectionForTheDatabaseTest())
            {
                await _respawner.ResetAsync(_configuration.Value.ConnectionString);
            }

        }

        public async Task InitializeAsync()
        {

            if (IsTheConnectionForTheDatabaseTest()) //اخطار بررسی شود حتما دیتابیس تست باشد چون تمامی دیتا ها پاک میشود با این دستور
            {
                _respawner = await Respawner.CreateAsync(_configuration.Value.ConnectionString,
                new RespawnerOptions
                {
                   CheckTemporalTables = true,
                    TablesToIgnore = new Respawn.Graph.Table[] { new("Roles") },
                    DbAdapter = DbAdapter.SqlServer
                }) ;
            }

        }

        private bool IsTheConnectionForTheDatabaseTest()
        {
            var connectionParts = _configuration.Value.ConnectionString.Split(";");
            var databaseName = connectionParts.FirstOrDefault(_ => _.Contains("database"));
            var server = connectionParts.FirstOrDefault(_ => _.Contains("server"));
            return databaseName != null && databaseName.Contains("test")
                && server != null && server.Split('=')[1] == ".";
        }
    }

}
