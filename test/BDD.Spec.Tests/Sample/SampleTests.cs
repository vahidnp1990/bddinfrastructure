﻿using BDD.Core.Domain;
using BDD.Core.DomainSarvices.People.Contracts.Dtos;
using BDD.Infrastructure.Persistence;
using BDD.Spec.Tests.Infrastructure;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Suzianna.Core.Events;
using Suzianna.Core.Screenplay;
using Suzianna.Core.Screenplay.Actors;
using Suzianna.Rest.Events;
using Suzianna.Rest.OAuth;
using System.Text;
using Xunit;

namespace BDD.Spec.Tests.Sample
{
    public class SampleTests : SpecPersistence
    {
        private readonly Actor _actor;
        public SampleTests(ConfigurationFixture<Program> configuration) : base(configuration)
        {
            _actor = Actor.Named("vahid");
            _actor.WhoCan(CallAnInMemoryRestApi.At("api/").With(_httpClient));
        }

        [Fact]
        private void Post_sample()
        {
            var Context = CreateDataBase<EFDataContext>().Context;
            Context.Roles.Add(new Role { Name = "xxxx" });
            Context.SaveChanges();
            var dto = new AddPersonDto
            {
                // Id = 1,
                Name = "test"
            };

            _actor.AttemptsTo(new AddPerson(dto));

            var expected =  CreateDataBase<EFDataContext>().Context.Set<Person>().SingleOrDefault();

            expected.Should().NotBeNull();
            expected!.Name.Should().Be(dto.Name);
        }

        [Fact]
        private async Task Put_sample()
        {
            var person = new Person { Name = "dummy_name" };
            CreateDataBase<EFDataContext>().Save(person);

            var dto = new UpdatePersonDto
            {
                Name = "dummy_update_name",
                Description = "description_update_dummy"
            };

            await Put("api/People/" + person.Id.ToString(), dto);

            var expected = CreateDataBase<EFDataContext>().Context.Set<Person>().SingleOrDefault();

            expected.Should().NotBeNull();
            expected!.Name.Should().Be(dto.Name);
        }

        [Fact]
        private async Task Patch_sample()
        {
            var person = new Person { Name = "dummy_name" };
            CreateDataBase<EFDataContext>().Save(person);

            var dto = new UpdatePersonNameDto
            {
                Name = "dummy_update_name"
            };

            await Patch("api/People/" + person.Id + "/name", dto);

            var expected = CreateDataBase<EFDataContext>().Context.Set<Person>().SingleOrDefault();

            expected.Should().NotBeNull();
            expected!.Name.Should().Be(dto.Name);
        }

        [Fact]
        private async Task Repository_Tests()
        {
            var Context = CreateDataBase<EFDataContext>();
            var person = new Person(new List<Custom> { new Custom { PersonId = 1 } }) { Name = "dummy_name" };
            Context.Save(person);
            var person1 = new Person(new List<Custom> { new Custom { PersonId = 2 } }) { Name = "dummy_name" };
            Context.Save(person1);
            var person2 = new Person(new List<Custom> { new Custom { PersonId = 3 } }) { Name = "dummy_name" };
            Context.Save(person2);
            var ids = new List<int>() { 1, 2 };
            var items =await Context.ReadContext.People.Where(_ => _.PersonIds.Any(c => ids.Any(_ => _ == c.PersonId))).ToListAsync();
        }
    }

    public class AddPerson : ITask
    {
        private readonly AddPersonDto dto;

        public AddPerson(AddPersonDto dto)
        {
            this.dto = dto;
        }

        public void PerformAs<T>(T actor) where T : Actor
        {
            actor.AttemptsTo(Post.Data(dto).To("People"));
            //long id = actor.AsksFor(LastResponse.Content<long>());
            //actor.Remember("PersonId", id);
        }
    }
    public class Post : IInteraction
    {
        private string _resource = default!;
        private readonly object _data;
        private Post(object data)
        {
            _data = data;
        }

        public static Post Data(object data)
        {
            return new Post(data);
        }

        public Post To(string resource)
        {
            _resource = resource;
            return this;
        }

        public void PerformAs<T>(T actor) where T : Actor
        {
            var ability = actor.FindAbility<CallAnInMemoryRestApi>();

            ability.SendRequest(_resource, _data);
        }

    }

    public abstract class HttpInteraction : IInteraction, IPerformable
    {
        private object _content = default!;

        public HttpInteraction WithContent(object content)
        {
            _content = content;
            return this;
        }

        public void PerformAs<T>(T actor) where T : Actor
        {
            CallAnInMemoryRestApi callAnApi = actor.FindAbility<CallAnInMemoryRestApi>();

            //callAnApi.SendRequest("api/People", SerializeObject(_content));
            Broadcaster.Publish(new HttpRequestSentEvent(callAnApi.LastResponse));
        }



        private static bool ActorHasAccessToken<T>(T actor) where T : Actor
        {
            return actor.CanRecall("SUZIANNA_OAUTH_TOKEN");
        }

        private static HttpRequestMessage AddAccessTokenToHeader<T>(T actor, HttpRequestMessage request) where T : Actor
        {
            OAuthToken oAuthToken = actor.Recall<OAuthToken>("SUZIANNA_OAUTH_TOKEN");
            request.Headers.Add("Authorization", oAuthToken.ToHeaderValue());
            return request;
        }


        public HttpInteraction WithHeader(string key, string value)
        {

            return this;
        }

        public HttpInteraction WithPostVerb()
        {
            return this;
        }
    }

    public class CallAnInMemoryRestApi : IAbility
    {
        private HttpClient _sender = default!;

        public string BaseUrl { get; private set; }

        public HttpResponseMessage LastResponse { get; private set; } = default!;

        private CallAnInMemoryRestApi(string baseUrl)
        {
            BaseUrl = baseUrl;
        }

        public static CallAnInMemoryRestApi At(string baseUrl)
        {
            return new CallAnInMemoryRestApi(baseUrl);
        }

        public CallAnInMemoryRestApi With(HttpClient sender)
        {
            _sender = sender;
            return this;
        }

        public void SendRequest(string resource, object data)
        {
            LastResponse = _sender.PostAsync(BaseUrl + resource, SerializeObject(data)).Result;
        }

        private static StringContent SerializeObject(object dto)
        {
            var bodyJson = JsonConvert.SerializeObject(dto);
            var stringContent = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            return stringContent;
        }
    }

}
