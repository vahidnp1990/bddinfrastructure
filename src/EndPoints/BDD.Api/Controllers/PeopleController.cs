﻿using BDD.Core.DomainSarvices.People.Contracts;
using BDD.Core.DomainSarvices.People.Contracts.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BDD.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PeopleController : ControllerBase
    {
        private readonly IPeopleService _peopleService;

        public PeopleController(IPeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        [HttpPost]
        public async Task<long> Add(AddPersonDto dto)
        {
           return await _peopleService.Add(dto);
        }

        [HttpPut("{id}")]
        public async Task Udpate(long id, UpdatePersonDto dto)
        {
            await _peopleService.Update(id, dto);   
        }

        [HttpPatch("{id}/name")]
        public async Task ChangeName(long id, UpdatePersonNameDto dto)
        {
            await _peopleService.ChangeName(id, dto);
        }
    }
}
