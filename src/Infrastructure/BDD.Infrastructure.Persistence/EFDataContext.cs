﻿using BDD.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BDD.Infrastructure.Persistence
{
    public class EFDataContext : DbContext
    {
        public EFDataContext(DbContextOptions<EFDataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EFDataContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Person> People { get; set; }
        public DbSet<Role> Roles { get; set; }
    }

    public class PersonMap : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> _)
        {
            _.ToTable("People")
                .HasKey(_ => _.Id);
            _.Property(_ => _.Name).HasMaxLength(255);
            _.Property(_ => _.Description).HasMaxLength(4000);
            _.OwnsMany(e => e.PersonIds, n => n.ToJson());
        }
    }
    public class PersonRole : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> _)
        {
            _.ToTable("Roles")
                .HasKey(_ => _.Id);
            _.Property(_ => _.Name).HasMaxLength(255);
        }
    }
}
