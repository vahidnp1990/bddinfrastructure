﻿using BDD.Core.Domain;
using BDD.Core.DomainSarvices.People.Contracts;

namespace BDD.Infrastructure.Persistence
{
    public class PeopleRepository : IPeopleRepository
    {
        private readonly EFDataContext _context;

        public PeopleRepository(EFDataContext context)
        {
            _context = context;
        }
        public void Add(Person dto)
        {
            _context.People.Add(dto);
        }

        public async Task<Person?> FindById(long id)
        {
            return await _context.People.FindAsync(id);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
