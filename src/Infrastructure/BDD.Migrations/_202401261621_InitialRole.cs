﻿using FluentMigrator;

namespace BDD.Migrations
{
    [Migration(202401261621)]
    public class _202401261621_InitialRole : Migration
    {
        public override void Down()
        {
            Delete.Table("Roles");
        }

        public override void Up()
        {
            
            Create.Table("Roles")
                   .WithColumn("Id").AsInt64()
                   .Identity().PrimaryKey()
                   .WithColumn("Name")
                   .AsString(20).NotNullable();
        }
    }
}
