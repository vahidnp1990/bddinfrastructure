﻿using FluentMigrator;

namespace BDD.Migrations
{
    [Migration(202207020856)]
    public class _202328100548_InitialPeople : Migration
    {
        public override void Down()
        {
            Delete.Table("People");
        }

        public override void Up()
        {
            
            Create.Table("People")
                   .WithColumn("Id").AsInt64()
                   .Identity().PrimaryKey()
                   .WithColumn("Name")
                   .AsString(20).NotNullable()
                   .WithColumn("Description")
                   .AsString(200).Nullable();
        }
    }
}
