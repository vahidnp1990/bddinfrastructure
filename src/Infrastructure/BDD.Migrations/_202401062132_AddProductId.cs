﻿using FluentMigrator;

namespace BDD.Migrations
{
    [Migration(202401062132)]
    public class _202401062132_AddProductId: Migration
    {
        public override void Down()
        {
            Delete.Column("PersonIds")
                .FromTable("People");
        }

        public override void Up()
        {

            Create.Column("PersonIds")
                .OnTable("People").AsString(200);
        }
    }
}
