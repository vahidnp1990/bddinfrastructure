﻿namespace BDD.Core.Domain
{
    public class Person
    {
        public Person()
        {
            _ids = new();
        }
        public Person(List<Custom> ids)
        {
            _ids = ids;
        }
        public long Id { get; set; }
        public string Name { get; set; } = default!;
        public string? Description { get; set; }
        private List<Custom> _ids = new();
        public IReadOnlyCollection<Custom> PersonIds { get { return _ids; } }
    }
    public class Custom
    {
        public int PersonId { get; set; }
    }
}


