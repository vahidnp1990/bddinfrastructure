﻿namespace BDD.Core.Domain
{
    public class Role
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
