﻿using BDD.Core.Domain;
using BDD.Core.DomainSarvices.People.Contracts;
using BDD.Core.DomainSarvices.People.Contracts.Dtos;
using BDD.Core.DomainSarvices.People.Exceptions;

namespace BDD.Core.DomainSarvices.People
{
    public class PeopleService : IPeopleService
    {
        private readonly IPeopleRepository _peopleRepository;

        public PeopleService(IPeopleRepository peopleRepository)
        {
            _peopleRepository = peopleRepository;
        }

        public async Task<long> Add(AddPersonDto dto)
        {
            var person = new Person
            {
                Name = dto.Name,
                Description = dto.Description
            };
            _peopleRepository.Add(person);

            await _peopleRepository.Save();
            return person.Id;
        }

        public async Task Update(long id, UpdatePersonDto dto)
        {
            var person = await _peopleRepository.FindById(id);
            StopIfPersonNotFound(person);

            person!.Description = dto.Description;
            person.Name = dto.Name;

            await _peopleRepository.Save();
        }

        public async Task ChangeName(long id, UpdatePersonNameDto dto)
        {
            var person = await _peopleRepository.FindById(id);
            StopIfPersonNotFound(person);
            person!.Name = dto.Name;

            await _peopleRepository.Save();
        }

        private static void StopIfPersonNotFound(Person? person)
        {
            if (person == null)
            {
                throw new PersonNotFoundException();
            }
        }
    }
}
