﻿using BDD.Core.DomainSarvices.People.Contracts.Dtos;

namespace BDD.Core.DomainSarvices.People.Contracts
{
    public interface IPeopleService
    {
        Task<long> Add(AddPersonDto dto);
        Task ChangeName(long id, UpdatePersonNameDto dto);
        Task Update(long id, UpdatePersonDto dto);
    }
}
