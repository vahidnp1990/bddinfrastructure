﻿namespace BDD.Core.DomainSarvices.People.Contracts.Dtos
{
    public class AddPersonDto
    {
        public string Name { get; set; } = default!;
        public string? Description { get; set; }
    }
}
