﻿namespace BDD.Core.DomainSarvices.People.Contracts.Dtos
{
    public class UpdatePersonDto
    {
        public string Name { get; set; } = default!;
        public string? Description { get; set; }
    }
}
