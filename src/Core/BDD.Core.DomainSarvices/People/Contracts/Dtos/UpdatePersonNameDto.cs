﻿namespace BDD.Core.DomainSarvices.People.Contracts.Dtos
{
    public class UpdatePersonNameDto
    {
        public string Name { get; set; } = default!;
    }
}
