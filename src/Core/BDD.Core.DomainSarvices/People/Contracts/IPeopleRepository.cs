﻿using BDD.Core.Domain;

namespace BDD.Core.DomainSarvices.People.Contracts
{
    public interface IPeopleRepository
    {
        void Add(Person dto);
        Task<Person?> FindById(long id);
        Task Save();
    }
}
